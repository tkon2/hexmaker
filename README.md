# Hexmaker

Generates an image of repeating hexagons


# How do hexagons work?

A regular hexagon's vertecies are essentially defined as 6 equally spaced
points on a circle. The relationship between this circle and the hexagon it
produces is as follows:
The circle's radius is equal to the hexagon's outer circumferance, or the distance
M to any other point in the visual below. This is refered to as the hexagon's R value. There
is also an equally important r value, which is the distance between M and the midpoint of any line segment. Essentially these define the hexagon's width and height. When the hexagon is drawn as below, the R value is the height, and the r value is the width. This is important for tiling.

```
          E
       *     *
    *           *
 D                 F
 *                 *
 *        M        *
 *                 *
 C                 A
    *           *
       *     *
          B
```

Working out these values from each other is relatively straight forward. There are several ways to go about it, but one of the easiest is this:

Take for instance the triangle A,F,(FA) where (FA) refers to the midpoint
between the two points F and A. This forms a right angled triangle with one known side,
either the hypotenuse or the segment M,(FA), and all angles are known. We know
the angles because each point is rotated 60 degrees from each other, since
360/6 (the number of sides) is 60. Given this, we know that the angle at M is
30, since the slice we are looking at is half of a side, so half of 30.
Incidentally we also know that the line segment F,A is exactly the same as
M,(FA), because the triangle M,F,A is a regular triangle. Regardless, our triangle now looks like this:


```
 * M           | Relationship between R and r
 |\            | ----------------------------
 | \           |
 |30\          | r / R = cos(M)
 |   \         |
 |    \ R      | Therefore:
r|     \       |
 |      \      | R = r / cos(M)
 |       \     | r = R * cos(M)
 |90    60\    |
 *---------*   | Where M = 30 degrees
F   r/2   (FA) |
```



