const width = 1080;
const height = 2400;
const heightAdjust = -20;
const cols = 8;
const gap = 12;
const border = 5;
const highlight = [4,16];
const highlightSize = 630;
let highlightColor = [80,80,80]; // Grey

// highlightColor = [256,20,10]; // Red
// highlightColor = [255,120,0]; // Orange
// highlightColor = [255,0,200]; // Purple
// highlightColor = [0,100,255]; // Blue
// highlightColor = [0, 255, 60]; // Green

const backgroundColor = [14,14,14];
const borderColor = [22,22,22];
const hexColor = [17,17,17];

function setup() {
  const toColor = (arr = [255,255,255]) => {
    return color(arr[0], arr[1], arr[2]);
  }

  createCanvas(width, height);
  background(toColor(backgroundColor));
  angleMode(DEGREES);

  const hexWidth = (width/cols) - gap;
  const hexHeight = (hexWidth/Math.sqrt(3));
  const hexRadius = hexHeight - border/2;
  const rowHeight = (hexWidth + gap) * cos(30);

  const drawHexagon = (x,y, bc = borderColor, fc = hexColor) => {
    fill(toColor(fc));
    stroke(toColor(bc));
    strokeWeight(border);
    beginShape();
    for (let a = 30; a < 360+30; a += 60) {
      let sx = x + cos(a%360) * hexHeight;
      let sy = y + sin(a%360) * hexHeight;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }

  const drawGradient = (x,y,r = highlightSize) => {
    noStroke();
    for (let i = r; i > 0; i--) {
      const v = i * (1/r);
      let inner = toColor(highlightColor);
      let outer = toColor(backgroundColor);
      const value = Math.pow(v, 1/3);
      fill(lerpColor(inner, outer, value));
      circle(x,y,i);
    }
  }

  const getPos = (x,y) => {
    const yOff = heightAdjust + -1 * hexHeight / 2;
    const dy = yOff + y * rowHeight;

    const oddOffset = y % 2 === 0
      ? gap/2 + hexWidth/2
      : 0;
    const regularOffset = hexWidth/2 + gap / 2;
    const xOff = regularOffset - oddOffset;
    const xDelta = hexWidth + gap;
    dx = xOff + xDelta * x;
    if (dx > (width - xOff) || dy > (height + rowHeight + gap)) {
      return [null, null];
    }
    return [dx, dy];
  }

  const [gx, gy] = getPos(highlight[0], highlight[1]);
  if (gx && gy) {
    drawGradient(gx,gy);
  }

  let x = 0,y = 0;
  while (true) {
    const [dx,dy] = getPos(x,y);
    if (dx == null || dy == null) {
      if (x === 0) {
        break;
      } else {
        x = 0;
        y += 1;
        continue;
      }
    }
    drawHexagon(dx,dy);
    x += 1;
  }

  if (gx && gy) {
    drawHexagon(
      gx,
      gy,
      highlightColor.map(c => Math.min(255, c * 1.3)),
      highlightColor.map(c => Math.max(0, c /7)),
    );
  }

  noLoop();
}

